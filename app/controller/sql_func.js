const conf = require('config');
const query = require('mysql-query-promise');
const check = require('./xss_check')
const tableName = conf.tableName;

const crud = {
    getAll: async () => {
        return query(`SELECT * from ${tableName}`);
    },
    get: async (id) => {
        let products = await query(`SELECT * FROM ${tableName} WHERE id=?`,[Number(id)]);
        return products[0];
    },
    getLast: async(last) => {
        let result = await query(`SELECT * from ${tableName} ORDER BY id DESC LIMIT ${last}`);
        return result;

    },
    create: async (username, email) => {
        console.log('create', username)
        let userToSave = {name: String(check(username)), email: String(check(email))};
        //if (id > 0) userToSave.id = Number(id);
        let result = await query(`INSERT INTO ${tableName} SET ? ON DUPLICATE KEY UPDATE ?`,[userToSave,userToSave]);
        console.log("INSERT", result.insertId)
    },
    createTable: async (tableName) => {
        await query(`CREATE TABLE IF NOT EXISTS ${tableName}(id INT primary key auto_increment, name VARCHAR(20), email VARCHAR(20))`)
    },

    update: async (id, product)=> {
        if (typeof product === 'object') {
            let uProduct = {};
            if (product.hasOwnProperty('name')) uProduct.name = String(product.name);
            if (product.hasOwnProperty('price')) uProduct.price = Number(product.price);
            if (product.hasOwnProperty('currency')) uProduct.currency = String(product.currency);
            let result = await query(`UPDATE ${tableName} SET ? WHERE id=?`,[uProduct, Number(id)]);
            return result.affectedRows;
        }
    },
    delete: async (id) => {
        let result = await query(`DELETE FROM ${tableName} WHERE id=?`,[Number(id)]);
        return result.affectedRows;
    }
};

function sayHallo(){
    console.log('func hello')
}

//module.exports = sayHallo();
module.exports = crud;