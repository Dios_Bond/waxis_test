const conf = require('config');
const express = require('express');
const app = express();
const mysql = require('mysql');
const crud = require('./controller/sql_func');
const routes = require('./router/routes');
const check = require('./controller/xss_check')


app.use(express())
app.set('view engine', 'ejs')

/*Connect To DB*/
const connection = mysql.createConnection({
    host: conf.database.master.host,
    database: conf.database.database,
    user: conf.database.master.user,
    password: conf.database.master.password,
    port: conf.database.master.port,
    //insecureAuth : true
});

connection.connect(function(err) {  
    if (err) {
        console.log("mysql db connection ERROR! ", err)
    }
    else  
    console.log("mysql db connected!");  
});  
  

/*Use for Create Table & insert DATA*/
//crud.createTable(conf.tableName)
// for (let i = 0; i < 15; i++){
//     crud.create("myname" + i, "myemail" + i + "@ukr.net");
// };

app.use('/', routes);

console.log(check("mail/\@ru<script>")) //when you need send or take data you need filter by this func 

app.listen(conf.server.port, function () {
    console.log('app start at port ', conf.server.port);
});