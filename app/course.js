var fetch = require('node-fetch')
const linkPB = "https://api.privatbank.ua/p24api/exchange_rates?json&date=";
let arr = [];
const day = 7; //count day to get data


/* function for create link */
function getLink(i){
    let d = new Date();
    let link = linkPB + (d.getDate()-i) + "." + d.getMonth() + "." + d.getFullYear(); 
    console.log(link);
    return link
}

/*function to get data from api url*/
function getData(i){
    let p = fetch(getLink(i))
    .then(res => {
        return res.json()
    })
    .then(data => {
        //console.log(data);
        return data
    })
    .catch((err) => {
        console.log("error!!! ", err)
    })
    arr[i] = p
};


for (let i = 0; i < day; i++){
    getData(i);
}

/*out JSON with all data*/
Promise.all(arr)
.then(function(val) {
    console.log("All Promise are resolved :) ");
    console.log(JSON.stringify(val))        //our JSON object
})
.catch(function(reason) {
    console.log("!!! Some of the promises err, reason is it ", reason);
})

// /*out JSON with all data*/
// let outAllPromises = Promise.all(arr);
// outAllPromises.then(function(val) {
//     console.log("All Promise are resolved :) ");
//     console.log(JSON.stringify(val))
// });
// outAllPromises.catch(function(reason) {
//     console.log("!!! Some of the promises err, reason is it ", reason);
// });

