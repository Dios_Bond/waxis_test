const express = require('express');
const router = express.Router();
let catalog = '../public/';

const crud = require('../controller/sql_func');


router.use(function (req, res, next) {
    console.log('Router add to app');
    next();
});

// Page for Data from MYSQL
router.get('/', function(req, res) {
    Promise.resolve(crud.getLast(10))   //To return 10 last users
    .then(function(val) {
        //console.log("Promise are resolved :) ");
        res.render((catalog+'index'), {'data': val});
    })
    .catch(function(reason) {
        console.error("By route '/' promises err, reason is it ", reason);
    })

});

module.exports = router;